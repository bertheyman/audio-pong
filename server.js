/* globals process:true, __dirname: true */

var express = require("express");
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var _ = require("underscore");

var Player = require('./classes/Player.js');
var Status = require('./classes/Status.json');

var port = process.env.PORT || 3000;

var players = [];
var couples = [];

app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket) {
	console.log("[Server] new socket with id " + socket.id);

	var player = new Player(socket.id);
	player.socket_id = socket.id;
	players.push(player);
	socket.emit("id", socket.id);
	io.emit('changedNumPlayers', players.length);

	socket.on('disconnect', function() {
		// Gestopte speler zoeken in array
		var currentPlayer = _.filter(players, function(player) {
			return player.socket_id === socket.id;
		});

		if(currentPlayer[0].status === Status.playing){
			// Als die aan het spelen was: tegenspeler verwittigen
							console.log('check passed');

			for(var i = 0; i<couples.length; i++){
				console.log(couples[i][1].socket_id);
				if(couples[i][1].socket_id === currentPlayer[0].socket_id){
					console.log('connection_lost');
					io.to(couples[i][0].socket_id).emit('connection_lost');
				}else if(couples[i][0].socket_id === currentPlayer[0].socket_id){
					console.log('connection_lost');
					io.to(couples[i][1].socket_id).emit('connection_lost');
				}
			}

			couples = _.filter(couples, function(currentcouple) {
				return (currentcouple[0].socket_id !== socket.id && currentcouple[1].socket_id !== socket.id);
			});
		}

		// Vervangt players array door eentje zonder speler die disconnect
		players = _.filter(players, function(player) {
			return player.socket_id !== socket.id;
		});

		io.emit('changedNumPlayers', players.length);

	});

	socket.on("search_player", function search_player(id){
		player.status = Status.searching;

		var searchingPlayers = _.filter(players, function(player) {
			return (player.status === 1 && player.socket_id !== socket.id && player.socket_id !== id);
		});
		if(searchingPlayers.length !== 0){
					console.log(searchingPlayers[0].socket_id);
		}
		var randomMax = searchingPlayers.length-1;
		if(randomMax >= 0){
			var i = Math.round(Math.random()*(randomMax));

			// Status aanpassen van spelers naar playing
			var playersToAdjust = _.filter(players, function(player) {
				return player.socket_id === socket.id || player.socket_id === searchingPlayers[i].socket_id;
			});
			for(var j = 0; j<playersToAdjust.length; j++){
				playersToAdjust[j].status = Status.playing;
			}
			couples.push(playersToAdjust);

			// Berichten uitsturen naar clients
			io.to(socket.id).emit('found_stranger', searchingPlayers[i].socket_id);
			io.to(socket.id).emit('send_ball');
			io.to(searchingPlayers[i].socket_id).emit('found_stranger', socket.id);

		}

	});

	socket.on("sendYPos", function sendYPos(arr){
		io.to(arr[0]).emit('send_stranger_ypos', arr[1]);
	});

	socket.on("send_score", function send_score(arr){
		var scores = [arr[1], arr[2]];
		io.to(arr[0]).emit('recieve_scores', scores);
	});

	socket.on("game_over", function sendYPos(arr){
		io.to(arr[0]).emit('game_over', arr[1]);

		couples = _.filter(couples, function(currentcouple) {
			return (currentcouple[0].socket_id !== socket.id && currentcouple[1].socket_id !== socket.id);
		});
	});

	socket.on("sendBallYPos", function sendYPos(arr){
		var posBall = [arr[1], arr[2]];
		io.to(arr[0]).emit('send_stranger_ball_ypos', posBall);
	});

});

server.listen(port, function() {
	console.log('Server listening at port ' + port);
});
