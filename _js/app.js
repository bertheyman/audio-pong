var game;
var Game = require('../classes/browser/Game.js');

function btnStartClicked(e){
	$('.intro').addClass('hidden');
	$('.searching').removeClass('hidden');

	game = new Game($('.container'));
}

function btnAgainClicked(e){
	//$('.game').addClass('hidden');
	$('.end').addClass('hidden');
	$('.searching').removeClass('hidden');
	$('.game').addClass('hidden');

	game.resetGameSettings();
	game.searchStranger();
	game.analyseSound();
}

function init(){
	console.log('[App] init');

	$('.startBtn').on('click', btnStartClicked);
	$('.btn_opnieuw').on('click', btnAgainClicked);

/*
	var font = new FontFace("Dosis", "url(fonts/dosis-bold-webfont.woff2)", {});
	font.ready().then(function() {
	  // font loaded.. swap in the text / define own behavior.
	});

	font.load();
*/
}

init();
