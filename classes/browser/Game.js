var Class = require('../core/Class');

module.exports = (function(){

	var Game = Class.extend({

		init: function($el){
			this.winMessages = [
			"You won! Invite the neighbours and celebrate.",
			"Score! What an unseen genius.",
			"You got it! What a beautiful voice timbre.",
			"Waauw! Got some singing lessons?",
			"Unbelievable! Prepare a victory dance."
			];
			this.loseMessages = [
			"That's a shame! Do something about it.",
			"Lost! There must be a corner to cry over there.",
			"No! Drink your grief or demand a rematch.",
			"Dreadful! Keep trying.",
			"Ow! Lost your voice?"
			];
			this.resetGameSettings();
			var canvas = document.getElementById('cnvs');

      // Socket zone
			this.socket = io("/");
			//this.socket = io("http://localhost");
			this.socket.on("id", (function (data) {
				this.socketid = data;
			}).bind(this));

			this.socket.on("found_stranger", (function(data){
				$('#cnvs').removeClass('hidden');
				$('.game').removeClass('hidden');
				$('.searching').addClass('hidden');
				this.stranger_id = data;

				this.sendYPosToStranger();
			}).bind(this));

			this.socket.on("send_stranger_ypos", (function(data){
				this.strangerCenterY = data;
			}).bind(this));

			this.socket.on("changedNumPlayers", (function(data){
				if(data == 1){
					$('.numPlayers')[0].innerHTML = data + " player active right now. All by yourself... Invite some friends to play with!";
				}else{
					$('.numPlayers')[0].innerHTML = data + " players active right now.";
				}
			}).bind(this));

			this.socket.on("connection_lost", (function(){
				console.log('ok');
				this.handleConnectionLost();
			}).bind(this));

			this.socket.on("recieve_scores", (function(scores){
				if(this.ping){
					var ping = new Audio('sounds/ping.mp3');
					ping.play();
				}else{
					var pong = new Audio('sounds/pong.mp3');
					pong.play();
				}

				this.ping = !this.ping;

				$('#punten_rechts')[0].innerHTML = scores[0];
			  $('#punten_links')[0].innerHTML = scores[1];
			}).bind(this));

			this.socket.on("game_over", (function(data){
				var win = false;
				if(data === this.stranger_id){
					win = true;
				}
			  this.handleGameOver(win);

			}).bind(this));

			this.socket.on("send_stranger_ball_ypos", (function(data){
				this.balxpos = canvas.width - data[0];
				this.balypos = data[1];
			}).bind(this));

			this.socket.on("send_ball", (function(){
				this.shouldSendBall = true;
			}).bind(this));

			console.log('[Game] init');
			this.$el = $el;
			this.$connect = $('.startBtn');

			// Geluid opvangen
			this.autoConnect();

      // Alles tekenen (klaarzetten)
      this.drawGame();

		},

		resetGameSettings: function(){
			this.strangerCenterY = -1;
			this.shouldSendBall = false;
			this.gameOver = false;
			this.ping = false;

			// Punten resetten
			$('#punten_links')[0].innerHTML = 0;
			$('#punten_rechts')[0].innerHTML = 0;

			// Canvas & counters
			var canvas = document.getElementById('cnvs');
      this.centerY = canvas.height / 2;
      this.counter = 0;

      // Snelheden balletje
			this.xspeed = 2;
			this.yspeed = 2;
			this.balxpos = 400;
			this.balypos = 300;

			// Instellingen staaf en bal
			this.staafLength = 200;
			this.staafWidth = 20;
			this.balStraal = 15;
		},

		searchStranger: function(){
			this.socket.emit("search_player", this.socketid);
		},

		sendGameOver: function(loser_id){
				var arr = [this.stranger_id, loser_id];
				this.socket.emit("game_over", arr);

				var win = true;
				if(loser_id === this.socketid){
					win = false;
				}

				this.handleGameOver(win);
		},

		sendYPosToStranger: function(){
				var arr = [this.stranger_id, this.centerY];
				this.socket.emit("sendYPos", arr);
		},

		drawGame: function(){

			// Canvas & stuff
			var canvas = document.getElementById('cnvs');
      var context = canvas.getContext('2d');
			context.clearRect(0, 0, canvas.width, canvas.height);

      var centerX = 10;
      var strangerCenterX = canvas.width - (10 + this.staafWidth);

      // Teken staafje
			context.beginPath();
      context.arc(centerX+this.staafWidth/2, this.centerY, this.staafWidth/2, 0, 2 * Math.PI, false);
      context.rect(centerX, this.centerY, this.staafWidth, this.staafLength-2*this.staafWidth);
      context.arc(centerX+this.staafWidth/2, this.centerY+this.staafLength-2*this.staafWidth, this.staafWidth/2, 0, 2 * Math.PI, false);
      if(this.counter%4 === 0){

				var modifier = Math.round(this.centerY * 100 / canvas.height);
      	var finetune = 72;
      	modifier = modifier - finetune;

      	this.r = 67 - modifier;
      	this.g = 83 - modifier;
      	this.b = 171 - modifier;

      	var yUsedForStranger;
      	if(this.strangerCenterY === -1){
      		yUsedForStranger = 430;
      	}else{
      		yUsedForStranger = this.strangerCenterY;
      	}
      	var strModifier = Math.round(yUsedForStranger * 100 / canvas.height);
      	strModifier = strModifier - finetune;
      	this.rStr = 247 - strModifier;
      	this.gStr = 86 - strModifier;
      	this.bStr = 179 - strModifier;
      }

      context.fillStyle = this.generateColor(this.r, this.g, this.b);
      context.fill();
      context.closePath();

      // Teken staafje tegenstander
      if(this.strangerCenterY !== -1){
	      context.beginPath();
	      context.arc(strangerCenterX+this.staafWidth/2, this.strangerCenterY, this.staafWidth/2, 0, 2 * Math.PI, false);
	      context.rect(strangerCenterX, this.strangerCenterY, this.staafWidth, this.staafLength-2*this.staafWidth);
	      context.arc(strangerCenterX+this.staafWidth/2, this.strangerCenterY+this.staafLength-2*this.staafWidth, this.staafWidth/2, 0, 2 * Math.PI, false);
	      context.fillStyle = this.generateColor(this.rStr, this.gStr, this.bStr);
	      context.fill();
	      context.closePath();
      }

      // Teken bal
      	var strokewidth = 7;
      	context.beginPath();
	      context.arc(this.balxpos, this.balypos, this.balStraal-strokewidth, 0, 2 * Math.PI, false);
	      context.fillStyle = '#fff';
	      context.strokeStyle = '#4454a6';

				context.lineWidth = strokewidth;
      	context.stroke();

	      context.fill();
	      context.closePath();

      if(this.shouldSendBall){
	      var arr = [this.stranger_id, this.balxpos, this.balypos];
				this.socket.emit("sendBallYPos", arr);
      }

      // Position bal
      if(this.shouldSendBall){
      	this.manageYPos();
				this.manageXPos();
      }

			// Doorsturen naar medespeler
      this.sendYPosToStranger();
      this.counter++;

		},

		manageYPos: function(){
			var canvas = document.getElementById('cnvs');

			this.balypos = this.balypos + this.yspeed;
      if(this.balypos < this.balStraal){
      	// Botst tegen bovenrand
      	this.balypos = this.balStraal;
      	this.yspeed = -this.yspeed;
      }else if(this.balypos > canvas.height-this.balStraal){
      	// Botst tegen onderrand
      	this.balypos = canvas.height-this.balStraal;
      	this.yspeed = -this.yspeed;
      }
		},

		sendScores: function(){
			var arr = [this.stranger_id, $('#punten_links')[0].innerHTML, $('#punten_rechts')[0].innerHTML];
			this.socket.emit("send_score", arr);
		},

		generateColor: function(r, g, b){
			return "rgb("+r+","+g+","+b+")";
		},

		updateGameSpeed: function(){
			// Random kans op snelheidsverhoging, komt steeds minder vaak voor
			if(Math.abs(this.xspeed) <= 8){
				var yourChances = Math.round(Math.random()*((Math.abs(this.xspeed)-1)*2));

				if(yourChances === 0){
					// Kortste strootje -> versnelling hoger
						if(this.xspeed < 0){
							this.xspeed--;
						}else{
							this.xspeed++;
						}

						if(this.yspeed < 0){
							this.yspeed--;
						}else{
							this.yspeed++;
						}
				}
			}
		},

		manageXPos: function(){
			var canvas = document.getElementById('cnvs');

			this.balxpos = this.balxpos + this.xspeed;
      if(this.balxpos < 10+this.staafWidth + this.balStraal){
      	// Links gebotst
      	if(this.balypos > this.centerY && this.balypos < this.centerY+this.staafLength){
      		if(this.counter > 25){
      			// Bal geraakt -> speel verder
      			var pong = new Audio('sounds/pong.mp3');
						pong.play();
						var puntenLinks = $('#punten_links')[0].innerHTML;
						puntenLinks++;
						$('#punten_links')[0].innerHTML = puntenLinks;
						this.sendScores();

      			this.xspeed = -this.xspeed;
      			this.counter =  0;
      			this.balxpos = 10 + this.staafWidth + this.balStraal;

      			// Snelheid verhogen
      			if(this.shouldSendBall){
      				this.updateGameSpeed();
      			}
      		}


      	}else{
      		// Gemist -> spel verloren
      		this.sendGameOver(this.socketid);
      	}
      }else if(this.balxpos > canvas.width-(this.balStraal + this.staafWidth + 10)){
      	// Rechts gebotst
      	if(this.balypos > this.strangerCenterY && this.balypos < this.strangerCenterY+this.staafLength){
      		if(this.counter > 25){
      			// Bal geraakt -> speel verder
      			var ping = new Audio('sounds/ping.mp3');
						ping.play();
						var puntenRechts = $('#punten_rechts')[0].innerHTML;
						puntenRechts++;
						$('#punten_rechts')[0].innerHTML = puntenRechts;
						this.sendScores();

      			this.xspeed = -this.xspeed;
      			this.counter =  0;
      			this.balxpos = canvas.width - (10 + this.staafWidth + this.balStraal);

      			// Snelheid verhogen
      			if(this.shouldSendBall){
      				this.updateGameSpeed();
      			}
      		}


      	}else{
      		// Gemist -> spel verloren
      		this.sendGameOver(this.stranger_id);
      	}
      }
      this.counter++;
		},

		handleGameOver: function(win){
			// Zichtbaarheid van onderdelen aanpassen
				$('.end').removeClass('hidden');
				$('#cnvs').addClass('hidden');

			this.shouldSendBall = false;
      this.xspeed = 0;
      this.yspeed = 0;
			this.gameOver = true;

			if(win){
				this.handleWin();
			}else{
				this.handleLose();
			}

			$('.endText').removeClass('hidden');

		},

		handleConnectionLost: function(){
			// Zichtbaarheid van onderdelen aanpassen
				$('.end').removeClass('hidden');
				$('#cnvs').addClass('hidden');

			this.shouldSendBall = false;
      this.xspeed = 0;
      this.yspeed = 0;
			this.gameOver = true;

			$('.endText')[0].innerHTML = "Je tegenstander zijn verbinding is verbroken.";
			$('.btn_opnieuw')[0].innerHTML = 'opnieuw';

			$('.endText').removeClass('hidden');

		},

		handleWin: function(){
			var textNumber = Math.round(Math.random()*(this.winMessages.length-1));
			$('.endText')[0].innerHTML = this.winMessages[textNumber];
			$('.btn_opnieuw')[0].innerHTML = 'again';
		},

		handleLose: function(){
			var textNumber = Math.round(Math.random()*(this.loseMessages.length-1));
			$('.endText')[0].innerHTML = this.loseMessages[textNumber];
			$('.btn_opnieuw')[0].innerHTML = 'revanche';
		},

		analyseSound: function(){
			this.analyser.getByteTimeDomainData(this.dataArray);

        var low = 0;
        var high = 0;
			for(var i = 0; i < this.bufferLength; i++) {

				if(i < this.bufferLength/2){
					low += this.dataArray[i] / 128.0;
				}else{
					high += this.dataArray[i] / 128.0;
				}

      }

      var difference = low - high;

      	if(Math.abs(difference) > 3){
      		        this.centerY = this.centerY - Math.abs(difference);

      		      }else{
      		      	        this.centerY = this.centerY + (3 - Math.abs(difference));

      		      }

        this.adjustCenterY();

        this.drawGame();

        if(!this.gameOver){
        	requestAnimationFrame(this.analyseSound.bind(this));
        }
		},

		adjustCenterY: function(){
			var canvas = document.getElementById('cnvs');
			if(this.centerY < this.staafWidth/2){
				// Bovenrand
        	this.centerY = this.staafWidth/2;
        }else if(this.centerY > canvas.height-(this.staafLength-this.staafWidth*3/2)){
        	// Onderrand
        	this.centerY = canvas.height-(this.staafLength-this.staafWidth*3/2);
        }
		},

		userMediaSuccesHandler: function(stream){
			console.log('[Game] success media');
			this.globStream = stream;

			// Zoeken in gang zetten
			$('.searchText')[0].innerHTML = "Searching for an opponent...";
		  this.searchStranger();

			// Context & analyser
			var context = new window.AudioContext();
			this.analyser = context.createAnalyser();

			// Input aan koppelen
			var source = context.createMediaStreamSource(stream);
			source.connect(this.analyser);
			var byteFrequency = new Uint8Array(this.analyser.frequencyBinCount);
			this.analyser.getByteTimeDomainData(byteFrequency);

			this.analyser.fftSize = 512;
			this.bufferLength = this.analyser.fftSize;
			this.dataArray = new Uint8Array(this.bufferLength);

      this.analyseSound();

		},

		userMediaErrorHandler: function(stream){
			console.log('[Game] error media');
		},

		autoConnect: function(){
			//Todo: audio afnemen
			navigator.getUserMedia = (navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

			if(navigator.getUserMedia){
				navigator.getUserMedia({
					audio: true,
					video: false
				},
				this.userMediaSuccesHandler.bind(this),
				this.userMediaErrorHandler.bind(this)
				);
			}
		},

	});


	return Game;

})();
