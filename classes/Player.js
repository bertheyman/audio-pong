var Class = require('./core/Class');
var Status = require('./Status.json');

module.exports = (function(){

	var Player = Class.extend({

		init: function(socket_id){
			this.socket_id = socket_id;
			this.status = Status.online;
		},

		set_status: function(status, socket){
			this.status = status;
			socket.emit("update_status", this.status);
		}

	});

	return Player;

})();
